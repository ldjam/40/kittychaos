local class = require 'lib.middleclass'
local vector = require "lib.hump.vector"

local ImageGroup = class('ImageGroup')

-- an image group may have: 
-- baseImageName
-- any number of directions (which are a mapping between angles and names)
-- states (any strings) 
-- animation states (numbered)

function ImageGroup:initialize(baseImageName, anchor, directions, states)
    assert(baseImageName)
    self.baseImageName = baseImageName
    self.directions = directions
    self.states = states
    self.anchor = anchor or vector(0, 0)
end

function ImageGroup:getImage(directionVector, state)
    -- if we have directions, find the best matching direction
    local bestDirection = nil
    local bestScalar = 0
    if self.directions then
        for name, vector in pairs(self.directions) do
            local scalar = directionVector:normalized() * vector:normalized()
            if scalar > bestScalar then
                bestScalar = scalar
                bestDirection = name
            end
        end
    end

    local name = self.baseImageName

    if bestDirection then
        name = name .. "_" .. bestDirection
    end

    if state then
        name = name .. "_" .. state
    end

    local retImage = images[name]
    local retAchor = nil
    if retImage == nil then
        print ("Did not find image " ..  name)
    else
        -- anchor coordinates can be absolute or reltative
        if self.anchor.x then -- relative
            retAnchor =  vector(self.anchor.x * retImage:getWidth(), self.anchor.y * retImage:getHeight())
        else -- absolute
            retAnchor = self.anchor
        end 
    end
    return retImage, retAnchor
end

ImageGroup.static.NESW = {  up    = vector( 0,-1), 
                            right = vector( 1, 0),
                            down  = vector( 0, 1),
                            left  = vector(-1, 0)
                         }


ImageGroup.static.eight = { n  = vector( 0,-1), 
                            ne = vector( 1,-1), 
                            e  = vector( 1, 0),
                            se = vector( 1, 1),
                            s  = vector( 0, 1),
                            sw = vector(-1, 1),
                            w  = vector(-1, 0),
                            nw = vector(-1,-1)
                         }
return ImageGroup
-- vim: ft=lua et ts=4 sw=4 sts=4
