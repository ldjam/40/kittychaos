require "lib.slam"
vector = require "lib.hump.vector"
Timer = require "lib.hump.timer"
Camera = require "lib.hump.camera"
tlfres = require "lib.tlfres"

require "helpers"

class = require 'lib.middleclass'
ImageGroup = require 'ImageGroup'
Object = require "Object"
Edible = require "Edible"
Creature = require "creatures.Creature"
TargetCreature = require "creatures.TargetCreature"
Coffee = require "creatures.Coffee"
Human = require "creatures.Human"
AutonomousHuman = require "creatures.AutonomousHuman"
Roomba = require "creatures.Roomba"
Cat = require "creatures.Cat"
SuperCat = require "creatures.SuperCat"
Dog = require "creatures.Dog"
Mouse = require "creatures.Mouse"
Grid = require ("lib.Jumper.jumper.grid") -- The grid class
Pathfinder = require ("lib.Jumper.jumper.pathfinder") -- The pathfinder lass

WORLD_HEIGHT = 12
WORLD_WIDTH = 16
TILE_SIZE = 16   -- pixel per tile
TILE_SCALE = 4   -- scale per tile
TILE_CENTER_OFFSET = TILE_SCALE * TILE_SIZE / 2
CANVAS_WIDTH = WORLD_WIDTH * TILE_SIZE * TILE_SCALE
CANVAS_HEIGHT = WORLD_HEIGHT * TILE_SIZE * TILE_SCALE

DOG_BASKET = vector(2,8)
DOG_BASKET_CENTER = DOG_BASKET + vector(0.7,0)

isFullscreen = false
phase = 1
doorOpen = false

printf = function(s,...)
    --print(s:format(...))
end 

-- need to change something to trigger a rebuild :/

function love.load()
    love.math.setRandomSeed(love.timer.getTime())

    -- load assets
    images = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("images")) do
        if filename ~= ".gitkeep" then
            images[filename:sub(1,-5)] = love.graphics.newImage("images/"..filename)
        end
    end

    sounds = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("sounds")) do
        if filename ~= ".gitkeep" then
            sounds[filename:sub(1,-5)] = love.audio.newSource("sounds/"..filename, "static")
        end
    end

    music = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("music")) do
        if filename ~= ".gitkeep" then
            music[filename:sub(1,-5)] = love.audio.newSource("music/"..filename)
            music[filename:sub(1,-5)]:setLooping(true)
        end
    end

    fonts = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("fonts")) do
        if filename ~= ".gitkeep" then
            fonts[filename:sub(1,-5)] = {}
            for fontsize=50,100 do
                fonts[filename:sub(1,-5)][fontsize] = love.graphics.newFont("fonts/"..filename, fontsize)
            end
        end
    end

    -- set up camera
    camera = Camera(0, 0)
    camera:zoom(1)

    -- set up default drawing options
    love.graphics.setBackgroundColor(0, 0, 0)


    worldWalkable = {}
    for x = 0, WORLD_WIDTH-1 do
        worldWalkable[x] = {}
        for y = 0, WORLD_HEIGHT-1 do
            worldWalkable[x][y] = 1
            if x <= 1 or y <= 1 or x >= WORLD_WIDTH-2 or y >= WORLD_HEIGHT-2 then
                worldWalkable[x][y] = 0
            end
        end
    end

    -- set up pathfinder
    grid = Grid(worldWalkable) 


    -- Creates a pathfinder object using Jump Point Search
    myFinder = Pathfinder(grid, 'ASTAR', 1) 
    myFinder:setMode('ORTHOGONAL')

    -- set up game objects
    objects = {}

    super_cat = SuperCat:new()
    super_cat.pos = vector(0,0)
    
    table.insert(objects, super_cat)
    table.insert(objects, Cat:new("black"))
    -- table.insert(objects, Cat:new("white"))
    -- table.insert(objects, Cat:new("grey"))
    
    -- objects.coffee = Coffee:new()
    -- objects.coffee.pos = vector(-200, 200)

    --objects.dog = Dog:new()
    --objects.dog.pos = vector(90,83)

    --objects.roomba = Roomba:new()
    --objects.roomba.pos = vector(50,-50)

    objects.human = AutonomousHuman:new()
    
    for _, object in pairs(objects) do
        object:logicUpdate()
    end

    Timer.every(5, addMouse)

    music.waiting_cat:setVolume(0.4)
    music.cat_house:setVolume(0.2)
    music.catter_house:setVolume(0.2)
    music.cattest_house:setVolume(0.2)
    setMusic(music.waiting_cat)    
end

function addMouse()
    print("MAAAAUUUUS!")
    mouse = Mouse:new()
    if  math.random( 0,100) > 50 then
        mouse.pos = vector(-385, 206)
    else
        mouse.pos = vector( 385, 206) -- the right hole has a collision problem
    end        
    table.insert(objects,mouse)
end

--
-- Simple collision detection code
--
local collisionMap = {}
collisionMap[Cat   ] = {}
collisionMap[Dog   ] = {}
collisionMap[Human ] = {}
collisionMap[Mouse ] = {}
collisionMap[Roomba] = {}

collisionMap[Human][Dog] = function(dt,c1,c2)
    -- c1.pos = vector(0,0)
end
collisionMap[Roomba][Cat] = function(dt,c1,c2)
        c1.target = nil
        c1.movement = -1 * c1.movement
        if c2:instanceOf(Cat) or c2:instanceOf(SuperCat) then
            c2:damage()
        end
        print ("invert roomba "..tostring(c1.movement))
end
collisionMap[Roomba][SuperCat ] = collisionMap[Roomba][Cat]
collisionMap[Roomba][Human ] = collisionMap[Roomba][Cat]
collisionMap[Roomba][Dog   ] = collisionMap[Roomba][Cat]
collisionMap[Roomba][Roomba] = collisionMap[Roomba][Cat]
collisionMap[Roomba][Mouse ] = collisionMap[Roomba][Cat]
collisionMap[Mouse ][Mouse ] = function(dt,c1,c2)
    c1:tryCopulate(dt, c2)
end

collisionMap[Dog][Cat] = function(dt,c1,c2)
    if c2:instanceOf(Cat) or c2:instanceOf(SuperCat) then
        if c1.state ~= "sleeping" then
            c2:damage()
            sounds.bark:setVolume(0.5)
            sounds.bark:play()
        end
    end
    print ("invert roomba "..tostring(c1.movement))
end
collisionMap[Dog][SuperCat ] = collisionMap[Dog][Cat]


COLLISION_COOLDOWN = 2
local lastCollision = {}

function collisionDetected(dt, c1, c2)
    --local key1 = c1.class.name .. "," .. c2.class.name
    --local key2 = c2.class.name .. "," .. c1.class.name

    assert(Human)
    assert(Dog)
    assert(collisionMap)
    assert(collisionMap[Human][Dog])
    assert(collisionMap[Roomba][Mouse])

    --print(("(%s,%s): %s"):format(tostring(c1.class), tostring(c2.class),
        --tostring(collisionMap[{c1.class, c2.class}])))

    if collisionMap[c1.class] and collisionMap[c1.class][c2.class] then
        if not lastCollision[c1] then lastCollision[c1] = {} end
        if not lastCollision[c2] then lastCollision[c2] = {} end
        if not lastCollision[c1][c2] then
            lastCollision[c1][c2] = COLLISION_COOLDOWN
            lastCollision[c2][c1] = COLLISION_COOLDOWN
        end

        --print(lastCollision[[c1][c2]])
        if lastCollision[c1][c2] < COLLISION_COOLDOWN then
            --print("throttle")
            lastCollision[c1][c2] = lastCollision[c1][c2] + dt
            lastCollision[c2][c1] = lastCollision[c1][c2]
        else
            --print("collision " .. tostring(c1) .. "," .. tostring(c2))
            lastCollision[c1][c2] = 0
            lastCollision[c2][c1] = 0
            collisionMap[c1.class][c2.class](dt, c1, c2)
        end
    elseif (not collisionMap[c1.class] or not collisionMap[c1.class][c2.class])
        and collisionMap[c2.class] and collisionMap[c2.class][c1.class] then
        --print("recurse")
        collisionDetected(dt, c2, c1)
    end
end

--
-- Update game state
--
function love.update(dt)
    if phase == 5 then
    Timer.update(dt)
        for _, object in pairs(objects) do
            object:update(dt)
        end

        -- collision detection
        for _,object1 in pairs(objects) do
            for _,object2 in pairs(objects) do
                if object1 ~= object2 then
                    if object1:distanceTo(object2) < 1 then
                        --print("DEBUG")
                        --object1.baseImageName = "dog"
                        --object2.speed = -object2.speed
                        collisionDetected(dt, object1, object2)
                    end
                end
            end
        end
    end

end

function love.keypressed(key)
    if key == "escape" or key == "q" then
        love.window.setFullscreen(false)
        love.event.quit()
    elseif key == "f" then
        isFullscreen = not isFullscreen
        love.window.setFullscreen(isFullscreen)
    end
end

function love.mouse.getPosition()
    return tlfres.getMousePosition(CANVAS_WIDTH, CANVAS_HEIGHT)
end

function love.mousepressed(x, y, button, touch)
    x, y = tlfres.getMousePosition(CANVAS_WIDTH, CANVAS_HEIGHT)

    -- calculate world coordinates
    wx, wy = camera:worldCoords(x, y, 0, 0, CANVAS_WIDTH, CANVAS_HEIGHT)
    mousePos = vector(wx,wy)

    printf("Mouse press at %d, %d", wx, wy)

    if phase < 5 then
        phase = phase+1
        if phase == 5 then
            setMusic(music.cat_house)  
        end
    else
            
        --print("vorvormauswahl "..tostring(mousePos))
        for _,object in pairs(objects) do
            --print("vormauswahl "..tostring((object1.pos - mousePos):len()))
            if (object.pos - mousePos):len() <= object.radius then
                --print("mausauswahl"..tostring(object))
                if object ~= super_cat then
                    super_cat:setTarget(object)
                    return
                end
                --object1.baseImageName = "dog"
                --object2.speed = -object2.speed
                --collisionDetected(dt, object1, object2)
            end
        end

        if super_cat then
            super_cat:setTarget(vector (wx, wy))
        end
    end
end

function pixelToTilePos(pixelPos)
    local tileX = math.floor((pixelPos.x + CANVAS_WIDTH /2) / (TILE_SIZE * TILE_SCALE))
    local tileY = math.floor((pixelPos.y + CANVAS_HEIGHT/2) / (TILE_SIZE * TILE_SCALE))
    return vector(tileX, tileY)
end

function tileToPixelPos(tilePos)
    local pixelX = tilePos.x * TILE_SIZE * TILE_SCALE - CANVAS_WIDTH/2
    local pixelY = tilePos.y * TILE_SIZE * TILE_SCALE - CANVAS_HEIGHT/2
    return vector(pixelX, pixelY)
end

function pixelToTilePos(pixelPos)
    local tileX = math.floor((pixelPos.x + CANVAS_WIDTH /2) / (TILE_SIZE * TILE_SCALE))
    local tileY = math.floor((pixelPos.y + CANVAS_HEIGHT/2) / (TILE_SIZE * TILE_SCALE))
    return vector(tileX, tileY)
end

function tileToPixelPos(tilePos)
    local pixelX = tilePos.x * TILE_SIZE * TILE_SCALE - CANVAS_WIDTH/2
    local pixelY = tilePos.y * TILE_SIZE * TILE_SCALE - CANVAS_HEIGHT/2
    return vector(pixelX, pixelY)
end

function drawTile(image, x, y, rotate, scalex, scaley)
    rotate = rotate or 0
    scalex = scalex or 1
    scaley = scaley or 1
    love.graphics.draw(image,
        x * TILE_SIZE * TILE_SCALE - CANVAS_WIDTH/2,
        y * TILE_SIZE * TILE_SCALE - CANVAS_HEIGHT/2,
        rotate, scalex, scaley,
        0, 0)
end

function drawCollisionTile(image, x, y, rotate, scalex, scaley, w, h)
    drawTile(image, x, y, rotate, scalex, scaley)
    w = w or 1
    h = h or 1
    for dx = 0, w - 1 do
        for dy = 0, h - 1 do
            --print ("dddd " .. tostring(x + dx) .. " , " .. tostring(y + dy))
            worldWalkable[x + dx][y + dy] = 0
        end
    end
end
--
-- Draw game state
--
function love.draw()
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.setBackgroundColor(128,128,128)
    tlfres.beginRendering(CANVAS_WIDTH, CANVAS_HEIGHT)

    if phase == 1 then
        love.graphics.draw(images.kitty_title, 0, 0)
        love.graphics.setFont(fonts.m5x7[100])
        love.graphics.printf("Kittychaos",0,530,840, "center")  
        love.graphics.setFont(fonts.m5x7[50])
        love.graphics.printf("by lenaschimmel, rohieb\nDasMaichen, winniehell\n& larkinia",250,610,670,"left")  
        love.graphics.printf("to continue\nclick somewhere",750,654,670,"left")  

    elseif phase == 2 then
        love.graphics.draw(images.story1, 0, 0)
        love.graphics.setFont(fonts.m5x7[50])
        love.graphics.printf("\"We'll be back in a minute!\", the big humans said. And left me, alone with the little human. Maybe I should find something to eat and take a little nap, and dream of being a great lion!" ,50,385,950,"left")  

    elseif phase == 3 then
        love.graphics.draw(images.story2, 0, 0)
        love.graphics.setFont(fonts.m5x7[50])
        love.graphics.printf("The little human likes to collect things. I hope it won't bring in too much useless or scary stuff!\nScary stuff like those loud dirt eating monsters. I guess they might be really dangerous for me, maybe even as dangerous as dogs. I'll better stay far away from them.\nI'll just take a nap. Yes, a long, nice nap..." ,50,385,950,"left")  

    elseif phase == 4 then
        love.graphics.draw(images.story3, 0, 0)
        love.graphics.setFont(fonts.m5x7[50])
        love.graphics.printf("Move the sleepy, hungry, little cat by pointing and clicking somewhere. Eat by clicking on something edible.\nThe dog and the vacuum cleaner might hurt you and take away one of your 9 lives.\nRestore lives by sleeping. To go to sleep, just find a safe place and stay there for a while." ,50,385,950,"left")  

    elseif phase == 6 then
        love.graphics.draw(images.story4, 0, 0)
        love.graphics.setFont(fonts.m5x7[50])
        love.graphics.printf("Phew. This whole situation is far too stressful for a little cat like me. I guess I'll take a walk. Maybe the big, reasonable humans will be back when I return. And in the meantime, the little human can play with its monsters." ,50,385,950,"left")  
        love.graphics.printf("Press ESC to exit game." ,550,654,950,"left")  

    elseif phase == 5 then
        camera:attach(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT, true)

        -- draw background
        for x = 0, WORLD_WIDTH-1 do
            for y = 0, WORLD_HEIGHT-1 do
                -- if worldWalkable[x][y] then
                --     love.graphics.setColor(255, 255, 255, 255)
                -- else
                --     love.graphics.setColor(255, 128, 255, 255)
                -- end
                drawTile(images.wood_tile, x, y)
            end
        end

        for x = 0, WORLD_WIDTH-1 do
            drawTile(images.wall_straight, x, 0, math.pi/2)
            drawTile(images.wall_straight_lt, x, 1)
            drawTile(images.wall_straight_lt, x, WORLD_HEIGHT-2)
            drawTile(images.wall_straight, x, WORLD_HEIGHT, -math.pi/2)
        end

        for y = 0, WORLD_WIDTH-1 do
            drawTile(images.wall_straight, 0, y)
            drawTile(images.wall_straight_lt, 1, y)
            drawTile(images.wall_straight_lt, WORLD_WIDTH-2, y)
            drawTile(images.wall_straight, WORLD_WIDTH, y, math.pi)
        end

        drawTile(images.wall_corner_u, 0, 0)
        drawTile(images.wall_corner_u, 0, WORLD_HEIGHT, -math.pi/2)
        drawTile(images.wall_corner_u, WORLD_WIDTH, 0, math.pi/2)
        drawTile(images.wall_corner_u, WORLD_WIDTH, WORLD_HEIGHT, math.pi)

        drawTile(images.wall_corner_l, 1, 1)
        drawTile(images.wall_corner_l, 1, WORLD_HEIGHT-1, -math.pi/2)
        drawTile(images.wall_corner_l, WORLD_WIDTH-1, 1, math.pi/2)
        drawTile(images.wall_corner_l, WORLD_WIDTH-1, WORLD_HEIGHT-1, math.pi)

        drawTile(images.wall_c1, 1, 0)
        drawTile(images.wall_c1, WORLD_WIDTH-1, 0, 0, -1) 
        drawTile(images.wall_c1, WORLD_WIDTH-1, WORLD_HEIGHT, math.pi)
        drawTile(images.wall_c1, 1, WORLD_HEIGHT, 0, 1, -1)
       
        drawTile(images.wall_c1, 0, 1, -math.pi/2, -1)
        drawTile(images.wall_c1, 0, WORLD_HEIGHT-1, -math.pi/2)
        drawTile(images.wall_c1, WORLD_WIDTH, 1, math.pi/2)
        drawTile(images.wall_c1, WORLD_WIDTH, WORLD_HEIGHT-1, math.pi/2, -1)

        drawTile(images.wall_c2, 2, 1)
        drawTile(images.wall_c2, WORLD_WIDTH-2, 1, 0, -1) 
        drawTile(images.wall_c2, WORLD_WIDTH-2, WORLD_HEIGHT-1, math.pi)
        drawTile(images.wall_c2, 2, WORLD_HEIGHT-1, 0, 1, -1)
       
        drawTile(images.wall_c2, 1, 2, -math.pi/2, -1)
        drawTile(images.wall_c2_mouse, 1, WORLD_HEIGHT-2, -math.pi/2)
        drawTile(images.wall_c2, WORLD_WIDTH-1, 2, math.pi/2)
        drawTile(images.wall_c2_mouse, WORLD_WIDTH-1, WORLD_HEIGHT-2, math.pi/2, -1)

        drawTile(images.door_new, WORLD_WIDTH, 5, math.pi/2)
        
        if doorOpen then
            drawTile(images.door_open, 7, WORLD_HEIGHT, math.pi)
        else
            drawTile(images.door_new, 7, WORLD_HEIGHT, math.pi)
        end
        drawTile(images.window, 6, 0)
        drawTile(images.window, 0, 7, -math.pi/2)
        drawTile(images.rug, 4, 5)
        drawTile(images.basket, DOG_BASKET.x, DOG_BASKET.y, -math.pi/2)


        drawCollisionTile(images.kitchen_dummy1, WORLD_WIDTH-7, 2, 0, 1, 1, 5, 1)
        drawCollisionTile(images.kitchen_dummy2, WORLD_WIDTH-3, 2, 0, 1, 1, 1, 3)

        for x = WORLD_WIDTH-7, WORLD_WIDTH-3 do
            for y = 2, 5 do
                drawTile(images.kitchen_tile, x, y)
            end
        end
        
        drawTile(images.kitchen, WORLD_WIDTH-7, 0)
        drawTile(images.kaffeemaschine, WORLD_WIDTH-3, 2)
        drawTile(images.cup, WORLD_WIDTH-4, 2)
        drawCollisionTile(images.bett, 2, 2, 0, 1, 1, 2, 3)
     
        drawCollisionTile(images.sofa_candle, WORLD_WIDTH-7, WORLD_HEIGHT-5, 0, 1, 1, 4, 3)
        drawCollisionTile(images.plant, WORLD_WIDTH-3, WORLD_HEIGHT-3)
        drawCollisionTile(images.desk, 5, 2, 0, 1, 1, 2, 2)

        if super_cat then
            
            if super_cat.targetPos and not super_cat.targetObject then
                local pos = super_cat:getTargetPos()
                love.graphics.draw(images.destination, pos.x - 32, pos.y - 32)
            end
        end

        -- draw world in ascending y order
        indices = {}
        for i,n in pairs(objects) do
            table.insert(indices, i)
        end
        table.sort(indices, function(i1,i2)
            return objects[i1].pos.y < objects[i2].pos.y
        end)

        for _,i in pairs(indices) do
            objects[i]:draw()
        end

        if doorOpen then
            drawTile(images.door_open_transparent, 7, WORLD_HEIGHT, math.pi)
        end

        camera:detach()

    end



    -- draw UI
    --love.graphics.print("energy: " .. objects.human.energy, 0, 0)

    if phase >= 5 then
        tx = 40
        ty = 10
        love.graphics.setColor(100, 100, 100)
        love.graphics.draw(super_cat.text, tx + 4, ty + 4)
        love.graphics.draw(super_cat.text, tx    , ty + 8)
        love.graphics.setColor(255, 255, 255)
        love.graphics.draw(super_cat.text, tx    , ty + 4)
    end

    tlfres.endRendering()
end

function removeFromFloor(objectToRemove)
    for i,o in pairs(objects) do
        if o == objectToRemove then
            objects[i] = nil
            return
        end
    end
end

function setMusic(music)
    if currentMusic == music then
        return
    end
    if soundtrack then
        soundtrack:pause()
    end
    currentMusic = music
    soundtrack = music:play()
end

-- vim: ft=lua et ts=4 sw=4 sts=6
