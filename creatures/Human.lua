local Human = class('Human', TargetCreature)

function Human:initialize()
    TargetCreature.initialize(self, 'Spieler')
    self.imageGroup = ImageGroup:new('child', vector(0.5, 1), ImageGroup.static.NESW, {} )
    self:setTarget(nil)
    self.radius = 4*8
    self.cargo = nil
    self.speed = 80
    self.energy = 100
    self.logicUpdateInterval = 0.3
end

function Human:selectImage()
    if self.cargo then
        return self.imageGroup:getImage(self.lookDirection, "armsup")
    else
        return self.imageGroup:getImage(self.lookDirection)
    end
end

-- update the direction based on the movement vector. don't change it if there is no movement. Might be needed for the carry cargo drawing.
function Human:updateDirectionFromVector()
    if not self.movement or self.movement:len() == 0 then return end -- nicht ändern, wenn wir uns nicht bewegen

    local angle = self.movement:toPolar().x
    local pi = 3.141592654
    if angle > pi*3/4 or angle < -pi*3/4  then
        self.direction = "_up" -- around -pi
    elseif angle > -pi*1/4 and angle < pi*1/4 then
        self.direction = "_down" -- around 0
    elseif angle > -pi*3/4 and angle < -pi*1/4 then
        self.direction = "_left" -- around -pi/2
    else
        self.direction = "_right" -- around pi/2
    end
end

function Human:update(dt)
    TargetCreature.update(self, dt)
    self.energy = self.energy - dt
end

function Human:handleTargetReached(reachedTarget)
    local targetObject = nil
    if not vector.isvector(reachedTarget) then
        targetObject = reachedTarget
    end

    -- reached a target object, let's pick it up
    if self.cargo and (self.shallDropCargo or targetObject) then
        self:dropCargo()
    end
    if targetObject then
        if targetObject:isInstanceOf(Edible) then
            print("ingested a "..tostring(targetObject))
            targetObject:ingest()
        else
            -- just pick it up
            self.cargo = targetObject
            self.cargo:handlePickUp()
            removeFromFloor(targetObject)
            self.targetObject = nil
        end
    end
end

function Human:dropCargo() 
    if self.cargo then
        self.cargo.pos = self.pos + self.lookDirection * 35
        table.insert(objects, self.cargo)
        self.cargo:logicUpdate()
        self.cargo = nil
    end
end

function Human:draw()
    if self.cargo == nil then
        TargetCreature.draw(self)
    else
        self:updateDirectionFromVector()

        local cargoImage, cargoAnchor = self.cargo.imageGroup:getImage(-self.lookDirection)
        local angle = 0
        local drawCargoFirst = false
        local vert = false
        
        if self.direction == "_left" then
            angle = -math.pi / 2
        end
        if self.direction == "_right" then
            angle = math.pi / 2
        end
        if self.direction == "_down" then
            angle = 0
            vert = true
        end
        if self.direction == "_up" then
            angle = math.pi
            drawCargoFirst = true
            vert = true
        end

        if not drawCargoFirst then TargetCreature.draw(self) end

        local carryHeight = 45
        local carryWidth  = 16
                        
        love.graphics.push()
        love.graphics.translate(self.pos.x, self.pos.y)
        love.graphics.translate(0, -carryHeight)
        love.graphics.rotate(angle)
        if self.direction == "_up" then
            love.graphics.translate(-cargoAnchor.x, -cargoAnchor.y/2+40)
        elseif self.direction == "_down" then
            love.graphics.translate(-cargoAnchor.x, -cargoAnchor.y/2+20)
        else
            love.graphics.translate(-cargoAnchor.x, -cargoAnchor.y)
            love.graphics.translate(0, -carryWidth)
        end

        love.graphics.draw(cargoImage, 0, 0, 0)
        love.graphics.pop()

        if drawCargoFirst then TargetCreature.draw(self) end
    end
end

return Human
-- vim: ft=lua et ts=4 sw=4 sts=4
