local Cat = class('Cat', TargetCreature)

-- sleeping
-- walking (just walking around)
-- scared
-- chasing (has target)
-- mating (has target)
-- eating (has target)
-- destroying (has target)

function Cat:initialize(color)
    TargetCreature.initialize(self, 'Katze', 'cat')
    if not color then
        colors = {"black", "white", "grey" }
        color = colors[math.random(1, #colors)]
    end
    self.instanceName = color .. " cat"
    self.imageGroup = ImageGroup:new('cat_' .. color, vector(0.5, 1), ImageGroup.static.NESW, {} )
    self:setTarget(nil)
    self.radius = 4*8
    self.logicUpdateInterval = 0.2
    self.state = "sleeping"
    self.timeSinceStateChange = 0
    self.color = { r = 255, g = 255, b = 255}
    font = love.graphics.newFont("fonts/poke.ttf", 32)
    self.text = love.graphics.newText(font)
    self.foodCount = 0
    self.damageCount = 0
    self:updateText()
end

function Cat:increaseFoodCount(inc) 
    inc = inc or 1
    self.foodCount = self.foodCount + inc
    self:updateText()
end

function Cat:draw()
    if self.food then
        love.graphics.draw(self.food:selectImage(), self.pos.x + 55, self.pos.y, math.pi, 1, 0.5)
    end
    love.graphics.setColor(self.color.r, self.color.g, self.color.b)
    TargetCreature.draw(self)
    love.graphics.setColor(255, 255, 255)

end

function Cat:update(dt)
    self.timeSinceStateChange = self.timeSinceStateChange + dt
    TargetCreature.update(self, dt)
end

function Cat:selectImage()
    if self.state == "sleeping" then
        return self.imageGroup:getImage(vector(1,0), "sleeping")
    elseif self.state == "eating" then
        -- a little animation
        if (self.timeSinceStateChange > 0.5 and self.timeSinceStateChange < 1)
            or (self.timeSinceStateChange > 1.5 and self.timeSinceStateChange < 2)
            or (self.timeSinceStateChange > 2.5 and self.timeSinceStateChange < 3)
            then
            return self.imageGroup:getImage(vector(1,0))
        else
            return self.imageGroup:getImage(vector(1,0), "eating")
        end
    else
        return self.imageGroup:getImage(self.lookDirection)
    end
end

function Cat:sleepingBehaviour()
    self:setTarget(nil)
    if self:shouldBeScared() then
        self:setState("scared")
    elseif self.timeSinceStateChange > 12 then
        self:setState("walking")
        if self.damageCount > 0 then
            self.damageCount = self.damageCount - 1
            self:updateText()
        end
    else
        self.movement = vector(0,0)
    end
end

function Cat:waitingBehaviour()
    self:setState("walking")
end

function Cat:walkingBehaviour()
    self.speed = 35
    dog = self:nearestObjectOfType(Dog, 150)
    if self:shouldBeScared() then
        self:setState("scared")
    elseif self.timeSinceStateChange > 12 then
        self:setState("sleeping")
    else
        mouse = self:nearestObjectOfType(Mouse, 200)
        if mouse then
            self:setState("chasing")
            self:setTarget(mouse)
        elseif self.targetPos == nil then
            repeat
                possibleTarget = self.pos + vector.randomDirection(120, 300)
            until self:testTileAtPixelPos(possibleTarget)
            self:setTarget(possibleTarget)
        end
    end
end

function Cat:scaredBehaviour()
    self.speed = 160
    dog, dogDist = self:nearestObjectOfType(Dog, 400)
    if not dog then
        self:setState("walking")
        print ("notdog")
    else
        if not self.targetPos or dogDist < 100 then
            local maxDist = 0
            for i=1,10 do
                local possibleTarget = self.pos + math.random(50, 200) * self.lookDirection + vector.randomDirection(50, 200)
                local dog, dogDist = self:nearestObjectOfType(Dog, 300, possibleTarget)
                local roomba, roombaDist = self:nearestObjectOfType(Roomba, 250, possibleTarget)
                local dangerDist = 1000
                if dog and dogDist < dangerDist then dangerDist = dogDist end
                if roomba and roombaDist < dangerDist then dangerDist = roombaDist end

                if self:testTileAtPixelPos(possibleTarget) and dangerDist > maxDist then
                    self:setTarget(possibleTarget)
                    maxDist = dangerDist
                end
            end
        end
    end
end

function Cat:chasingBehaviour()
    ta = self:getTarget()
    if not ta then
        self:setState("walking")
    else
        dist = self:distanceTo(ta)
        if dist < 30 then
            self.movement = vector(0,0)
            removeFromFloor(ta)
            self:setState("eating")
            sounds.cat_eating:setVolume(0.2)
            sounds.cat_eating:play()
            if ta:instanceOf(Mouse) then
                sounds.mouse:play()
            end
            self:increaseFoodCount()
            self.food = ta
            ta.dead = true
            self:setTarget(nil)
        elseif dist < 70 then
            self.speed = 400
        elseif dist < 150 then
            self.speed = 150
        else
            self.speed = 50
        end
    end
end

function Cat:eatingBehaviour()
    if self.timeSinceStateChange > 4 then
        self:setState("walking")
        self.food = nil
    else
        self.movement = vector(0,0)
    end
end

function Cat:logicUpdate(dt)
    --print (self.instanceName .. " status " .. self.state)
    if self.state == "sleeping" then
        self:sleepingBehaviour()
    elseif self.state == "waiting" then
        self:waitingBehaviour()
    elseif self.state == "walking" then
        self:walkingBehaviour()
    elseif self.state == "scared" then
        self:scaredBehaviour()
    elseif self.state == "chasing" then
        self:chasingBehaviour()
    elseif self.state == "mating" then

    elseif self.state == "eating" then
        self:eatingBehaviour()
    elseif self.state == "destroying" then

    end

    -- if self:isStuck() then 
    --     self:setState("walking")
    --     Creature.logicUpdate(self)
    --     print("B")
    -- end
end

function Cat:setState(newState)
    --self:printf(debug.traceback())

    if self.state == newState then return end

    self:printf ("Changed state from %s to %s.", self.state, newState)
    
    self.state = newState
    self.timeSinceStateChange = 0
    self.timeSinceLogicUpdate = 1000
    if newState ~= "eating" then
        self.food = nil
    end
end

function Cat:shouldBeScared()
    dog = self:nearestObjectOfType(Dog, 120)
    roomba = self:nearestObjectOfType(Roomba, 80)
    return (dog or roomba)
end

function Cat:handlePickUp()
    sounds.meow:setPitch(math.random(90,130)/100)
    sounds.meow:setVolume(0.5)
    sounds.meow:play()
    self:setState("walking")
end

function Cat:damage()
    self.damageCount = self.damageCount + 1
    self:updateText()
    if self:instanceOf(SuperCat) then
        sounds.outch:setVolume(0.9)
        if self.damageCount >= 9 then
            phase = 6
            setMusic(music.waiting_cat) 
            -- this should mute the roombas
            roombas = self:nearObjectsOfType(Roomba)
            for k,v in pairs(roombas) do
                v.object:update(1)
            end
        end
    else
        sounds.outch:setVolume(0.3)
    end
    sounds.outch:play()
    self:setState("scared")
end

function Cat:updateText()
    self.text:set(string.format("     Score: %d                         Lives: %d", self.foodCount, 9 - self.damageCount))
end

return Cat
-- vim: ft=lua et ts=4 sw=4 sts=4
