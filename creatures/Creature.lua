local Creature = class('Creature', Object)

function Creature:initialize(name)
    Object.initialize(self, name)
    self.movement = vector(0, 0.01)
    self.lookDirection = vector(0, 1)
    self.speed = 40
end

function Object:selectImage()
    return self.imageGroup:getImage(self.lookDirection)
end

function Creature:update(dt)
    assert(self.pos) 

    if self.movement:len2() > 0 then
        self.lookDirection = self.movement:normalized()

        -- detect collisions, perform movement if nothing collides
        local newPos = self.pos + self.movement * dt
        local testPos = self.pos + self.lookDirection * self.radius
        local tileTest = self:testTileAtPixelPos(testPos)
        local stuck = self:isStuck()

        if not tileTest and stuck then -- we are stuck, allow movement
            tileTest = true
        end

        if tileTest then
            self.pos = newPos
        else
            self.movement = vector(0,0)
            self:handleWallCollision()
        end
    end
    
    assert(self.pos) 

    -- will perform logicUpdate, if needed
    Object.update(self, dt)
end

function Creature:randomWalkablePosition()
    repeat
        possibleTarget = vector(math.random(-CANVAS_WIDTH, CANVAS_WIDTH), math.random( -CANVAS_HEIGHT, CANVAS_HEIGHT))
    until self:testTileAtPixelPos(possibleTarget)
    return possibleTarget
end

function Creature:testTileAtPixelPos(testPos) 
    local tile = pixelToTilePos(testPos)
    return tile.x >= 0 and tile.y >= 0 and tile.x < WORLD_WIDTH and tile.y < WORLD_HEIGHT and worldWalkable[tile.x][tile.y] == 1
end

function Creature:isStuck()
    return not self:testTileAtPixelPos(self.pos)
end

function Creature:handleWallCollision()
    self:logicUpdate()
end

function Creature:handleObjectCollision(object)
    self:logicUpdate()
end

return Creature
-- vim: ft=lua et ts=4 sw=4 sts=4
