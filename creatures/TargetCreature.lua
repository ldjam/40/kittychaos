local TargetCreature = class('TargetCreature', Creature)

function TargetCreature:initialize(name)
    Creature.initialize(self, name)
    self.speed = 40
end

function TargetCreature:selectImage()
    return self.imageGroup:getImage(self.lookDirection)
end

function TargetCreature:update(dt)
    assert(self.pos)

    if self.targetObject and self.targetObject.dead then
        self:setTarget(nil)
    end

    if self.targetObject and (self.targetObject.pos - self.targetPos):len() > 5 then
        self:setTarget(self.targetObject)
    end

    -- try to get unstuck, if stuck at all
    if not self:testTileAtPixelPos(self.pos) then
        radius = 1
        repeat
            testPos = self.pos + vector.randomDirection(radius)
            radius = radius + 0.3
        until self:testTileAtPixelPos(testPos)
        self.pos = testPos
        self:printf("got unstuck")
    end

    -- try to find directTarget
    if self.path then
        if self.targetPos and self:isDirectlyWalkable(self.targetPos) then
            self.directPathTarget = self.targetPos
            --self:printf("Direct path to target is walkable")
        else
            local found = false
            for i=1,#self.path do
                local num = #self.path - i + 1
                if not found and self:isDirectlyWalkable(self.path[num]) then
                    self.directPathTarget = self.path[num]
                    --self:printf("Found directly reachable point on path: %d of %d", num, #self.path)
                    found = true
                end
            end
            if not found then
                self.directPathTarget = nil
            end
        end
    end

    targetPos = self.directPathTarget -- might be just the next "Zwischenziel"

    if targetPos then
        local diff = (targetPos - self.pos)
        local dist = diff:len()
        if dist > 0.1 then
            self.lookDirection = diff:normalized()
        end

        if dist < self.speed * dt then -- target so close that we reach it in this frame
            self.pos = targetPos
        else -- target too far to reach it in this frame
            self.movement = diff:normalized() * self.speed 
            self.pos = self.pos + self.movement  * dt
        end   
    else
        --print "Nowhere to go!"
    end
    assert(self.pos) 

    -- check the "endgültiges ziel" here
    if self.targetPos and (self.pos - self.targetPos):len() < 2 then
        local priorTarget = self.targetObject or self.targetPos
        targetPos = nil
        self:setTarget(nil)
        self:handleTargetReached(priorTarget)
    end
    -- will perform logicUpdate, if needed
    Object.update(self, dt)
end

function TargetCreature:handleTargetReached(target)

end

function TargetCreature:getTarget()
    if self.targetObject then
        return self.targetObject
    else
        return self.targetPos
    end
end

function TargetCreature:getTargetPos()
    if self.targetObject then
        return self.targetObject.pos
    elseif vector.isvector(self.targetPos) then
        return self.targetPos
    else
        return nil
    end
end

function TargetCreature:setTarget(newTarget)
    if newTarget == nil then
        self.targetObject = nil
        self.targetPos = nil
        self.directPathTarget = nil
        self.path = nil
        self.movement = vector(0,0)
        return
    end

    if vector.isvector(newTarget) then
        self.targetPos = newTarget
        self.targetObject = nil
    else
        self.targetPos = newTarget.pos
        self.targetObject = newTarget
    end

    local tilePos = pixelToTilePos(self.pos)
    local tileTarget = pixelToTilePos(self.targetPos)

    local path = myFinder:getPath(tilePos.y, tilePos.x, tileTarget.y, tileTarget.x)
    if path then
        self.path = {}
        self.pathIndex = 1
        --printf('Path found! Length: %.2f', path:getLength())
        for node, count in path:nodes() do
            --printf('Step: %d - x: %d - y: %d',count, node:getX(), node:getY())
            local tileVec = vector(node:getY(), node:getX())
            local pixelVec = tileToPixelPos(tileVec) + vector(TILE_CENTER_OFFSET, TILE_CENTER_OFFSET)
            table.insert(self.path, pixelVec)
        end
        table.insert(self.path, self.targetPos)
    else
        printf("No path from %d, %d to %d, %d", tilePos.x, tilePos.y, tileTarget.x, tileTarget.y)
        if worldWalkable[tileTarget.x][tileTarget.y] == 0 then -- invalid target
            self:setTarget(nil)
        end
    end
end

-- function TargetCreature:draw()
--     Creature.draw(self)
--     if self.path then
--         love.graphics.setColor(255,0,0)
--         for i=1,#self.path-1 do
--             love.graphics.line(self.path[i].x, self.path[i].y, self.path[i+1].x, self.path[i+1].y)
--         end
--         love.graphics.setColor(255,255,255)
--     end
    
--     if self.directPathTarget then
--         --print("DIRECT")
--         love.graphics.setColor(0,255,255)
--         love.graphics.line(self.pos.x, self.pos.y, self.directPathTarget.x, self.directPathTarget.y)
--         love.graphics.setColor(255,255,255)
--     end
-- end

function TargetCreature:isStuck()
    return not self:testTileAtPixelPos(self.pos)
end

-- assign a random target position
function TargetCreature:logicUpdate()
end

function TargetCreature:handleWallCollision()
    self:setTarget(nil)
    self:logicUpdate()
end

function TargetCreature:handleObjectCollision(object)
    self:logicUpdate()
end

function TargetCreature:isDirectlyWalkable(pos)
    local diff = pos - self.pos
    local dist = diff:len()
    local steps = math.floor(dist / 10) + 1
    for i=1,steps do
        local test = self.pos + diff * i / steps
        love.graphics.setColor(0,0,255)
        for i=1,#self.path-1 do
            love.graphics.circle("fill", test.x, test.y, 4)
        end
        love.graphics.setColor(255,255,255)
        if not self:testTileAtPixelPos(test) then
            return false
        end
    end
    return true
end

return TargetCreature
-- vim: ft=lua et ts=4 sw=4 sts=4
