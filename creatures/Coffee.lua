Coffee = class('Coffee', Edible)

function Coffee:initialize()
    Object.initialize(self, "anonymous coffee")
    self.imageGroup = ImageGroup:new('cup', vector(0.5, 0.5))
    self.radius = 3*8
    self.nutritionalValue = 50
end

return Coffee
-- vim: ft=lua et ts=4 sw=4 sts=4
