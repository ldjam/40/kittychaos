local Object = class('Object')

function Object:initialize(instanceName)
    self.instanceName = instanceName
    self.pos = vector(math.random() * 600 - 300, math.random() * 600 - 300)
    self.anchor = vector(0,0)
    self.radius = 1
    self.logicUpdateInterval = 0.5
    self.timeSinceLogicUpdate = math.random(0, self.logicUpdateInterval)
end

function Object:selectImage()
    return self.imageGroup:selectImage()
end

-- draw the object with it's (selected) image, or if there is no image, use the instanceName as dummy
function Object:draw()
    --love.graphics.setColor(255,255,255)
    local image, anchor = self:selectImage()
    assert(image)
    assert(anchor)

    love.graphics.push()
        love.graphics.translate(self.pos.x, self.pos.y)
        love.graphics.push()
            love.graphics.translate(-anchor.x, -anchor.y)
        
            if image then
                love.graphics.draw(image)
            else
                love.graphics.print(self.instanceName)
            end
        love.graphics.pop()

        -- draw target marker on the flor
        if self == super_cat.targetObject then
            love.graphics.setColor(0,255,0)
            love.graphics.draw(images.arrow_white, -32, -100)
            love.graphics.setColor(255,255,255)
        end
    love.graphics.pop()
end

-- execute logicUpdate if needed
function Object:update(dt)
    -- Logikupdate ausführen, falls es schon wieder so weit ist
    self.timeSinceLogicUpdate = self.timeSinceLogicUpdate  + dt
    if self.timeSinceLogicUpdate  > self.logicUpdateInterval then
        self:logicUpdate()
        self.timeSinceLogicUpdate = 0
    end
end


function Object:logicUpdate()
end

function Object:instanceOf(classObject)
    -- TODO has to check superclass hierarchy, but doesn't
    return self.class and self.class.name == classObject.name
end

function Object:randomObjectOfType(classObject, maxDist, position)
    local objs = self:nearObjectsOfType(classObject, maxDist, position)
    if #objs == 0 then return end
    local rand = math.random(1, #objs)
    return objs[rand].object, objs[rand].dist
end

function Object:nearObjectsOfType(classObject, maxDis, positiont)
    local maxDist = maxDist or 10000
    local found = {}
    position = position or self.pos
    for _,object in pairs(objects) do
        local dist = (position - object.pos):len()
        if self ~= object and
            dist < maxDist and
            object:instanceOf(classObject) then
            table.insert(found, {object = object, dist = dist})
        end
    end
    return found
end

function Object:nearestObjectOfType(classObject, maxDist, position)
    local minDist = maxDist or 10000
    local nearest = nil
    position = position or self.pos
    for _,object in pairs(objects) do
        local dist = (position - object.pos):len()
        if self ~= object and dist < minDist and object:instanceOf(classObject) then
            minDist = dist
            nearest = object
        end
    end
    return nearest, minDist
end

function Object:distanceTo(otherObject)
    if vector.isvector(otherObject) then
        return (self.pos - otherObject):len() -- - self.radius
    else
        if otherObject == nil or otherObject.pos == nil then return nil end
        return (self.pos - otherObject.pos):len() - (self.radius + otherObject.radius)
    end
end

function Object:handlePickUp()
end

function Object:printf(format, ...)
    printf((self.instanceName or "NONAME") .. ": " .. format, ...)
end

return Object
-- vim: ft=lua et ts=4 sw=4 sts=4
